from dnn import *

# For a neural network NN, perform gradient descent with fixed step epsilon, we cut the training set in groups of size k
def simpleTraining(NN,X,Y,k,epsilon):
    nbTrain = len(X)
    N = nbTrain
    c = 0
    while nbTrain > 0:
        os.system('clear')
        print(100*c/N)
        x = random.choice(X)
        y = x
        print(NN.crossEntropy(y,NN.propagation(x)))
        sizeGroup = min(k,nbTrain)
        gW,gB = NN.backpropagation(X[c],Y[c])
        c+=1
        nbTrain-=1
        for i in range(sizeGroup-1):
            gW1,gB1 = NN.backpropagation(X[c],Y[c])
            c+=1
            nbTrain -=1
            for i in range(len(gW)):
                gW[i] += gW1[i]
                gB[i] += gB1[i]

        for i in range(len(gW)):
            gW[i] = (1/sizeGroup)*gW[i]             
            
        for i in range(len(gB)):
            gB[i] = (1/sizeGroup)*gB[i]

        NN.update(gW,gB,epsilon)
            
def generateExemples(N):
    X = []
    Y = []
    choices = [np.matrix([[1],[0],[0]]),np.matrix([[0],[0],[1]])]
    for i in range(N):
        c = random.choice(choices)
        X.append(c)
        Y.append(c)
    return X,Y

def test(NN,N):
    X,Y = generateExemples(N)
    for i in range(len(X)):
        print("############")
        print(Y[i],NN.propagation(X[i]))


def generateSin(N):
    X = []
    Y = []
    for i in range(N):
        x = np.random.rand(1,1)
        X.append(x)
        Y.append(np.sin(x))
    return X,Y

def test2(NN,N):
    X,Y = generateSin(N)
    for i in range(len(X)):
        print("############")
        print(Y[i],NN.propagation(X[i]))
    


