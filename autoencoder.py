import numpy as np
import os as os
import random

class AUTOENCODER(object):

    def activation(self,x): # Activation function of the neurons, for now, a sigmoid
        if self.a == 0:
            return 1/(1+np.exp(-x))

    def dactivation(self,x): # Derivate of the activation
        if self.a == 0:
            return np.multiply(self.activation(x),(1-self.activation(x)))

    # Compute the cross-entropy between the correct output y, and the actual output a
    def crossEntropy(self,y,a):
        res = np.sum(-(np.multiply(y,np.log(a))+ np.multiply((1-y),np.log(1-a))))
        return res

    def __init__(self,layers,a=0):
        self.a = a # The type of the activation function we use
        self.layers = layers
        self.nbLayers = len(layers)
        self.nbInputs = layers[0]
        self.nbOutputs = layers[-1]
        self.weights = [np.random.rand(layers[i+1],layers[i]) for i in range(self.nbLayers-1)] # random initialisation of the matrices
        self.biaises = [np.random.rand(layers[i+1],1) for i in range(self.nbLayers-1)] # random initialisation of the biaises
    
    def propagation(self,x):
        res = np.copy(x)
        for i in range(0,self.nbLayers-1):
            res = self.activation(self.weights[i]@res + self.biaises[i])
        return res

    def encode(self,x,l):
        res = np.copy(x)
        for i in range(0,l):
            res = self.activation(self.weights[i]@res + self.biaises[i])
        return res

    def decode(self,x,l):
        res = np.copy(x)
        for i in range(l,self.nbLayers-1):
            res = self.activation(self.weights[i]@res + self.biaises[i])
        return res
    
    # Computing the gradient by backpropagation for input x, correct output y
    def backpropagation(self,x,y):
        gradWeights = [np.zeros(a.shape) for a in self.weights]
        gradBiaises = [np.zeros(a.shape) for a in self.biaises]

        # Forward pass to compute all activation functions
        z = [np.zeros((self.layers[i],1)) for i in range(self.nbLayers)]
        a = [np.zeros((self.layers[i],1)) for i in range(self.nbLayers)]
        z[0] = np.copy(x)
        a[0] = np.copy(x)
        for i in range(self.nbLayers-1):
            z[i+1] = self.weights[i]@a[i] + self.biaises[i]
            a[i+1] = self.activation(z[i+1])
        #end forward pass

        # Computing the error terms, i.e ∂E/∂z, for all neurons z
        errors = [np.zeros((self.layers[i],1)) for i in range(self.nbLayers)]
        errors[-1] = -(y-a[-1])

        for i in range(self.nbLayers-2):
            errors[-(i+2)] = np.multiply((np.transpose(self.weights[-(i+1)])@errors[-(i+1)]),self.dactivation(z[-(i+2)]))
        # End computation of the errors terms
            
        # Computing the gradient for the matrices ∂E/∂W
        for i in range(len(gradBiaises)):
            gradBiaises[i] = np.copy(errors[i+1])
            
        # Computing the gradient for the biaises ∂E/∂b
        for i in range(self.nbLayers-1):
            gradWeights[i] = errors[i+1]@np.transpose(a[i])

        return gradWeights,gradBiaises

    # Gradient checking, to see if the backpropagation is well implemented
    def checkGradient(self,x,y,epsilon):
        gW,gB = self.backpropagation(x,y)
        Estart = self.crossEntropy(y,self.propagation(x))
        for l in range(len(self.weights)):
            for i in range(len(self.weights[l])):
                for j in range(len(self.weights[l][0])):
                    ancien = self.weights[l][i,j]
                    self.weights[l][i,j] += epsilon
                    Efinal = self.crossEntropy(y,self.propagation(x))
                    self.weights[l][i,j] = ancien
                    print("#################")
                    print((Efinal-Estart)/epsilon)
                    print(gW[l][i,j])
                    print("#################")

        for l in range(len(self.biaises)):
            for i in range(len(self.biaises[l])):
                for j in range(len(self.biaises[l][0])):
                    ancien = self.biaises[l][i,j]
                    self.biaises[l][i,j] += epsilon
                    Efinal = self.crossEntropy(y,self.propagation(x))
                    self.biaises[l][i,j] = ancien
                    print("#################")
                    print((Efinal-Estart)/epsilon)
                    print(gB[l][i,j])
                    print("#################")

                    
    # Given a direction (gradient) and a size-step epsilon, update the neural network
    def update(self,gW,gB,epsilon):
        for i in range(len(gW)):
            self.weights[i] = self.weights[i] - epsilon*gW[i]

        for i in range(len(gB)):
            self.biaises[i] = self.biaises[i] - epsilon*gB[i]
